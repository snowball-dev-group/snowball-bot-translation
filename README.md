# Snowball Bot Translation

## About the repository

This repository contains the rules for the Snowball Translaion Project available on Crowdin:

https://crowdin.com/project/snowball-bot/

Rules are based per-languages, these rules apply only to translations.

Members behaviour is regulated by the Code of Conduct in Snowball Bot main repository.

## Basic rule of translations

See [BASIC RULES](/BASIC-RULES.md) document describing basic rules of translating the project.

## Contributing the rules

Learn how to contribute to rules in the [CONTIRUBING](/CONTRIBUTING.md) document.