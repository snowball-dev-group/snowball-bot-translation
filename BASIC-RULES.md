# Basic rules of translation

Generally, you're free to translate however you want until there is set of official rules. But we would like you to follow these basic rules of translation.

## 1. Use simple language

- Do not write or use hard and not frequenly understandable words.

- Check other popular applications, including Discord to see what word they use.

## 2. Make it native

- Make your translation sound like native. Totally rephrase the string but keep it sound like it is spoken or wrote by native speaker, do not translate word-for-word.

- This rule also sets you to use any native characters and orphography.

## 3. Translator for reference

- Do not under any circumstances insert “raw” translations from the translators such as Google Translate. Use it only for your knowledge or reference.

## 4. Stick with one translation style

- When you start translating the project from the scratch, you have a choice to either translate the project in formal or informal style.

  The differences that the formal style is more bussiness-like, when informal is like you are speaking to your friend.

  If you preferred informal, don't forget to keep funny emotions.

- You can also request us to add an other style version.

## 5. Learn ICU

- You may see such things as `{messages, plural, one {# message} other {# messages}}`. This is what called Plurals, there as you see, depending on number, it selects which word to use, in English `one` case is used only for `1`, `#` is being replaced with the nubmer.

- Whenever you don't need plurals, you can make them formatted numbers or just raw strings (which is not recommended) — `{messages, number}` (number) or `{messages}` (raw string).

---

Know how to improve this document? Submit a [merge request](https://www.thinkful.com/learn/github-pull-request-tutorial/) or notify us on our Discord servers (links can be found on Crowdin page). Thanks!