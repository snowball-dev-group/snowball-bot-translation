# Contribution Guide

Welcome to small contribution guide on rules for languages.

## How to submit a contribution?

Any contributions done using Merge Requests.

You need to fork the project, make required changes and then create Merge Request.

## How do I make a contribution?

Rules contribute to translation style and terms.

### Finding a placement of rules files

#### Rules for new languages

1. To create rules for languages that have no rules, you'll need to create a folder with language name in English in folder `Languages`.
2. Second, create a file called RULES.md, you allowed to create any other files as rules grow to describe random translations tools.

#### Rules for current languages

Go to `Languages` folder and find folder for your language, all folders have names of languages in English (Russian, French).

### Expectations from rules

Rules should be argumented using any sources or users feedback.

Examples:

- In Russian, we use quotation marks such as `«„“»`, see [Quotation Marks in Russian](https://ru.wikipedia.org/wiki/%D0%9A%D0%B0%D0%B2%D1%8B%D1%87%D0%BA%D0%B8) — **correct rule**

- In Russian, we type Microsoft as M$ because I hate this company — **absolutely incorrect**

### What you also can contribute?

- You can upload a translation of our Code of Conduct from the Snowball Bot repository so the other people without really good English knowledge can know how to behave while working on the project.

- We would like to have you describe your team members about such things such as ICU variables like Plurals.

### Who approves the changes?

The language proofreader. If there are no proofreader for you language, the Merge Request is stalled. You still able to manage your local branch with other people from the translation team. Once there is proofreader they will check your Merge Request and suggest changes or deny them.